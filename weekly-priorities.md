### Week of Dec 29th (3 day week)
- [ ] Release post duties
- [ ] Token OKR
- [ ] Data course
- [ ] Cleanup backlog
- [ ] Diagram out current state of AuthZ
- [ ] Review onboarding workflow

### Week of 2024-12-08
- [X] Planning issue
- [X] Release post
- [X] Organization problem valdiation on internal permission support and create internal survey questions
- [X] Experimental instructions for job tokens and admin custom role
- [ ] Cleanup backlog
- [X] Release post manager shadow
- [X] Data course
- [X] Volunteer with local GitLab team

### Week of 2024-11-18
- [X] Data request for attrition
- [ ] Draft up multi-stage AuthZ direction
- [X] Customer interviews for Admin Custom Role
- [X] Schedule local community event
- [X] Update product OKR for AuthZ
- [X] L&D Plan

### Week of 2024-10-28
- [X] Capture feedback for "Assign custom roles to groups"
- [X] Finalize permission category direction
- [X] Permit io webinar
- [X] Merge Token Permissions feature/tier
- [X] Determine experimental / beta / ga timeline for permissions for job tokens
- [ ] Start multi-stage direction for Authz
- [ ] Permission best practices section in docs

### Week of 2024-10-20
- [X] Customer interviews
- [X] Framework review
- [X] Solution validation review for onboarding
- [X] Kickoff permission direction updates
- [ ] Assign custom roles to groups feedback

### Week of 2024-10-08
- [X] Catch up from leave!
- [ ] Set up customer interviews

### Week of 2024-08-18 (3 day week)
- [X] Customer interviews
- [X] Tiering discussion for fine-grained permissions
- [X] Collaborate on design doc for job tokens
- [X] Fill out OOO leave issue
- [X] Govern vision workshop
- [X] Token survey
- [X] Permission table docs improvement
- [X] Set up token interviews

### Week of 2024-06-24
- [X] Customer interviews
- [ ] Research themes for AuthZ
- [X] Anti-abuse next iteration prep
- [ ] Async workshop prep for AuthZ
- [X] Permission table docs improvement
- [ ] Update pricing/feature pages

### Week of 2024-06-10 (4 days)
- [X] Customer interviews
- [X] View role and assigned users: Tidy up epics, tiering discussion
- [X] Priorities
- [X] Release posts
- [X] Anti-abuse tasks
- [X] Documentation OKR
- [ ] Update pricing/feature pages

### Week of 2024-05-20
- [X] Customer interviews
- [X] Record and share roadmap
- [X] Anti-Abuse Tasks
- [X] Documentation OKR
- [ ] Standards and consolidation planning
- [X] Organize epics/backlog
- [X] Complete continuous interview course

### Week of 2024-04-22
- [X] Customer interviews
- [X] AA quicksheet
- [X] Review custom role inheritance SV questions
- [X] Validation review for permissions: registry and project planning
- [ ] Create dogfooding issue and identify key requirements
- [X] Continuous interviewing course
- [ ] Teams next steps
- [ ] View MRs permission next steps
- [ ] Roadmap deck updates


### Week of 2024-04-15
- [X] Review AA it.1
- [X] Customer reachout
- [X] Summarize survey findings
- [ ] General estimation for roadmap
- [X] Doc improvements: direct/indirect, known issues, SAML group link
- [ ] Validaiton plan for permission groupings
- [X] Continuous interviewing course
- [X] Minimal access next steps
- [ ]Teams next steps

### Week of 2024-04-08
- [X] Customer interviews
- [X] Priorities page
- [X] Release notes
- [X] Update deprecation notes with instance api 
- [X] 1 year roadmap 
- [X] Organize epics 

### Week of 2024-04-01
- [X] Customer interviews
- [ ] FY25 roadmap review and recording
- [X] Test cases for role creation at instance
- [ ] Direct/indirect inheritance doc update
- [X] KR account update
- [X] Granular token review
- [X] Sync with Tenant team
- [ ] Features page update
- [X] Get up to speed on Anti-Abuse
- [ ] Organize backlog

### Week of 2024-03-25
- [X] Customer outreach
- [X] Revisit customer requests
- [ ] Organize backlog
- [ ] Send reminder of custom role deprecations to customer slack channels
- [X] Review "Run pipeline" permissions permissions review
- [ ] Review group and project setting permissions
- [X] Next steps for base role, REST API, allow/exception, auto-generated docs
- [ ] Sync with Tenant team
- [X] Data review
- [ ] Features page update

### Week of 2024-03-19 (3 days)
- [X] Catch up after summit
- [X] CDF review
- [ ] Send reminder of custom role deprecations to customer slack channels
- [X] Org workshop part 4


### Week of 2024-03-03
- [X] Prep release posts
- [X] Prep priorities file
- [X] Org workshop
- [ ] Finalize issues for reduction of Owner and Maintainer permissions
- [X] Finalize 'view' requirements for assigned permissions in custom roles
- [ ] Notify customers of deprecations
- [X] Customer interviews
- [ ] CDF review
- [ ] Organize backlog

### Week of 2024-02-20
 - [X] Review UXR Survey
 - [ ] Finalize issues for reduction of Owner and Maintainer permissions
 - [X] Collect reporting challenges on access/permissions
 - [ ] Notify customers of deprecations
 - [ ] CDF review
 - [ ] Enablement bites
 - [X] Customer interviews
 - [X] Look at summit events
 - [ ] Organize backlog

### Week of 2024-02-12
 - [X] Continue Onboarding
 - [X] Customer interviews
 - [X] Organization OST and roadmap into themes
 - [X] Continue organizing epics + issues for custom role permissions and notify respective PMs
 - [X] Gather feedback on direction page
 - [X] Data request on permissions
 - [X] Social outreach on custom roles
 - [X] Review membership management
 - [ ] Notify customers of deprecations
 - [ ] Look at summit events
 - [ ] Organize backlog

### Week of 2024-02-05
 - [X] Continue Onboarding
 - [X] Organize epics + issues for custom role permissions and notify respective PMs
 - [X] Writing up vision and direction for Authorization Group
 - [X] Customer interviews
 - [ ] Organize backlog

### Week of 2023-01-29
 - [X] Continue Onboarding
 - [X] Update custom roles documentation and related areas
 - [X] Publish blog for custom roles (submitted)
 - [X] 16.10 P1/P2 Prep
 - [X] Customer interviews
 - [ ] Organize epics + issues and identify dependencies

### Week of 2024-01-22
 - [X] Continue Onboarding
 - [X] Distribute Build Your Own Role Survey
 - [X] Customer interviews
 - [X] UXR survey review
 - [ ] Resarch and validate terraform state protection
 - [X] Resarch and validate registry protection
 - [X] Develop delivery approach to grouping permissions across product groups

### Week of 2024-01-15
 - [X] Continue Onboarding
 - [ ] 16.10 P1/P2 Prep
 - [X] Customer and internal user interviews
 - [X] Finalize questions for UX Research
 - [X] Finalize permissons survey and gather feedback. Distribute on Thursday.
 - [X] Continue requirements for Permissions [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/12264)

### Week of 2024-01-08
 - [X] Continue Onboarding
 - [X] 16.9 P1/P2 Prep
 - [X] 16.8 Release Posts
 - [X] UX Research Approach on Consolidating and Grouping Permissions
 - [ ] Draft Survey for Permissions and gather team feedback. Plan to share with customers next week.
 - [X] Sync with PMs on permissions for input and validation (code review and registry)
 - [ ] Continue requirements for Permissions [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/12264)

### Week of 2024-01-02 
- [X] Continue Onboarding
- [X] 16.9 P1/P2 Prep
- [ ] Organize customer request priority with RICE Framework
    - [X] Requests
    - [X] Reach
    - [ ] Impact
    - [ ] Confidence
    - [ ] Effort, First Pass
    - [X] Summary + Insights
- [ ] January Validation Prep
  - [X] Draft Customer Interview Questions
  - [ ] Draft Survey for Role Persona 
- [ ] [Complete Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/12264)
  - [X] Continue to build out requirements
  - [X] Wrap up architecture diagrams and gather feedback


### Week of 2023-12-25 (Short holiday week)
- [X] Continue Onboarding
- [X] 16.9 P1/P2 Prep
- [X] Organize customer request priority with RICE Framework


### Week of 2023-12-18
- [X] Continue Onboarding
- [X] Reviewing current GitLab permissions and understand customer gaps + opportunities
- [X] Coffee Chats with the team
- [X] Lite assessment of competitors
- [ ] 16.9 P1/P2 Prep
- [X] Begin to build out [Complete Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/12264) for Permissions Category

